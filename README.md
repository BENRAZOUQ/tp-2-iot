Compiler le code python permet de créer un fichier .csv qui contient les donnés longitude et latitude .

Description du code (la meme dans le rapport):

Importation des modules struct et csv : Ces modules sont utilisés pour manipuler respectivement des données binaires et pour écrire des données dans des fichiers CSV.

Définition de la fonction lire_trames(nom_fichier_binaire, nom_fichier_csv) : Cette fonction prend deux arguments, le nom du fichier binaire à lire et le nom du fichier CSV dans lequel écrire les données.

Définition du format de la structure formatS : Le code définit un format de structure en utilisant struct.Struct, indiquant la disposition des données dans une trame. Ce format est utilisé pour extraire les données de la trame binaire.

Ouverture des fichiers avec des contextes with : Les fichiers binaire et CSV sont ouverts dans des contextes with, ce qui garantit que les fichiers seront fermés correctement après leur utilisation.

Positionnement dans le fichier binaire : Le code se positionne au début de la première trame en utilisant la méthode seek() sur le fichier binaire.

Écriture de l'en-tête dans le fichier CSV : Le code écrit l'en-tête 'Longitude', 'Latitude' dans le fichier CSV en utilisant csv.writer.writerow().

Lecture et écriture des données de la trame : Le code utilise une boucle while pour lire les données de chaque trame du fichier binaire. Il vérifie si la première valeur de la trame est différente de 239 pour déterminer la fin du fichier. Les données de la trame sont extraites à l'aide de formatS.unpack(), puis converties en longitude et latitude. Enfin, les valeurs sont écrites dans le fichier CSV.

Appel de la fonction lire_trames() avec les noms des fichiers appropriés : La fonction est appelée avec le nom du fichier binaire à lire et le nom du fichier CSV dans lequel écrire les données.

En résumé, ce code lit les trames à partir d'un fichier binaire, extrait la longitude et la latitude de chaque trame, puis écrit ces données dans un fichier CSV. 
