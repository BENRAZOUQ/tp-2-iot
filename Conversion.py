import struct
import csv

def lire_trames(nom_fichier_binaire, nom_fichier_csv):
    formatS = struct.Struct("<26B 2I 15B")    
    
    with open(nom_fichier_binaire, 'rb') as fichier_binaire, open(nom_fichier_csv, 'w', newline='') as fichier_csv:
        fichier_binaire.seek(0x248)

        writer = csv.writer(fichier_csv)
        writer.writerow(['Longitude', 'Latitude'])  

        while True:
            donnees_trame = fichier_binaire.read(formatS.size)  
            
            if not donnees_trame or donnees_trame[0] != 239:
                break
            
            trame = formatS.unpack(donnees_trame)  
            longitude = trame[26]*10**(-7)
            latitude = trame[27]*10**(-7)
            writer.writerow([longitude, latitude])  

lire_trames("13caaf25c97e29c8b45ae2e4f9f8ac3b_T0_2e526", "Donnes.csv")
